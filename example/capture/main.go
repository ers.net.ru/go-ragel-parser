package main

import (
	"bytes"
	"github.com/goccy/go-graphviz"
	"log"
	"os/exec"
)

func main() {
	dot, err := exec.Command("ragel", "-V", "-Z", "-p", "atoi.rl").Output()

	if err != nil {
		log.Fatal(err)
	}

	g := graphviz.New()

	graph, err := graphviz.ParseBytes(dot)

	//_ =  .AddEdge(1, 2, graph.EdgeAttribute("color", "red"))

	firstNode := graph.FirstNode()
	//
	//next := first)

	graph.FirstEdge(firstNode).SetColor("red")

	g1, err := graph.Node("1")

	_ = g1

	graph.NextNode(graph.NextNode(firstNode)).SetColor("red")

	// 1. write encoded PNG data to buffer
	var buf bytes.Buffer
	if err := g.Render(graph, graphviz.SVG, &buf); err != nil {
		log.Fatal(err)
	}

	//// 2. get as image.Image instance
	//image, err := g.RenderImage(graph)
	//if err != nil {
	//	log.Fatal(err)
	//}

	// 3. write to file directly
	if err := g.RenderFilename(graph, graphviz.SVG, "graph.svg"); err != nil {
		log.Fatal(err)
	}
}
