## Ragel

[Official](http://www.colm.net/open-source/ragel/)

[Github](https://github.com/adrian-thurston/ragel)

[Ragel State Machine Compiler User Guide](https://www.colm.net/files/ragel/ragel-guide-6.10.pdf)

[Guide](https://github.com/kbandla/ragel/blob/master/doc/ragel-guide.txt)


## Alternative - re2c

[https://re2c.org/manual/manual_go.html](https://re2c.org/manual/manual_go.html)

`re2c` is a tool for generating fast lexical analyzers for C, C++ and Go. v3 add support Rust

[Benchmark](https://re2c.org/benchmarks/benchmarks.html)

## ldetool

[https://sirkon.github.io/ldetool/](https://sirkon.github.io/ldetool/)

`ldetool` is a command line utility to generate Go code for fast log files parsing.


## ragel-go

[https://github.com/db47h/ragel](https://github.com/db47h/ragel)

This package provides a driver for ragel based scanners in Go for streamed input.

It takes care of all the boilerplate code, letting the user focus on the ragel state machine definition.

## RubyConf 2015

[https://youtu.be/Tr83XxNRg3k](https://youtu.be/Tr83XxNRg3k)

[Ragel Playground](https://github.com/ijcd/ragel_playground)

[https://github.com/whitequark/parser/blob/d5d648ad072297e75bcaf267c11ce23e0470d50f/lib/parser/lexer.rl](https://github.com/whitequark/parser/blob/d5d648ad072297e75bcaf267c11ce23e0470d50f/lib/parser/lexer.rl)

[Ryby http simple server](https://github.com/evan/mongrel/tree/master/ext/http11)

[Ryby next https server](https://github.com/defunkt/unicorn/tree/master/ext/unicorn_http)

[Puma web server](https://github.com/puma/puma/tree/b6ef31ef7dfc0f3d2c5b3c320a2783e06bf4d96e/ext/puma_http11)

[RedCloth - Textile parser for Ruby](https://github.com/jgarber/redcloth/tree/master/ragel)

[Hpricot is a fast, flexible HTML parser written in C](https://github.com/hpricot/hpricot/tree/master/ext/hpricot_scan)


## go-syslog

[https://github.com/influxdata/go-syslog](https://github.com/influxdata/go-syslog)

A parser for Syslog messages and transports.

## rjson

[https://github.com/WillAbides/rjson](https://github.com/WillAbides/rjson)

rjson is a json parser that relies on Ragel-generated state machines for most parsing. rjson's api is minimal and focussed on efficient parsing.

[Discuss with link cpp](https://github.com/adrian-thurston/ragel/discussions/66)


### Ragel: state machine compiler

[Статья](https://ekhabarov.com/post/ragel-state-machine-compiler/)

[Код](https://github.com/ekhabarov/blog-code-snippets/tree/master/ragel)

[https://github.com/szhukovks/ragel-parser](https://github.com/szhukovks/ragel-parser)


### Other codes

[http and smtp perser](https://github.com/yaoweibin/nginx_tcp_proxy_module/tree/master/parsers)

[php-parser](https://github.com/z7zmey/php-parser/tree/master/internal/scanner)

[https://stackoverflow.com/questions/73461066/ragel-parser-is-not-greedy](https://stackoverflow.com/questions/73461066/ragel-parser-is-not-greedy)

[Error stack Ragel 6.10](https://stackoverflow.com/questions/48517257/what-is-the-correct-way-to-use-a-stack-with-a-scanner-block-in-ragel)

[https://github.com/gebv/ragel-yacc-example](https://github.com/gebv/ragel-yacc-example)