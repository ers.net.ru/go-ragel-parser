package ragelparser

import (
	"testing"
)

//go:generate go test -bench=. -benchmem strdelim_bench_test.go

func BenchmarkParseStrDelim(b *testing.B) {
	var (
		input = []byte(`123\ 456\|789\ яяя`)
		m     = new(Machine)
	)

	for i := 0; i < b.N; i++ {
		ssToString(ParseStrDelim(m, input))
	}
}
