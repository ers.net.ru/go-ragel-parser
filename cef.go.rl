package ragelparser

//go:generate ragel -Z -G2 -o cef.go cef.go.rl

%%{
  machine cef;


  # sets the start point for the current value
  action mark {
    m.pb = m.p
  }

  # cef.Version = (int(fc) - '0')
  action set_version{
    cef.Version = string(m.data[m.pb:m.p+1])
  }

  action set_device_vendor {
    cef.DeviceVendor = string(m.data[m.pb:m.p+1])
  }

  action set_device_product {
    cef.DeviceProduct = string(m.data[m.pb:m.p+1])
  }

  action set_device_version {
    cef.DeviceVersion = string(m.data[m.pb:m.p+1])
  }

  action set_device_event_class_id {
    cef.DeviceEventClassId = string(m.data[m.pb:m.p+1])
  }

  action set_name {
    cef.Name = string(m.data[m.pb:m.p+1])
  }

  action set_severity {
    cef.Severity = string(m.data[m.pb:m.p+1])
  }

  action set_k {
    k = m.string()
  }

  action set_v {
      v = m.string()
  }

  action set_ext {

    cef.Extensions[k]=v
  }

  sp = ' ';
  cln = ':';
  vl = '|';

  gr = (any - vl)+{1,64};
  # gr = (graph - (vl)){1,64};

  version = digit >mark @set_version;
  device_vendor = gr >mark @set_device_vendor;
  device_product = gr >mark @set_device_product;
  device_version = gr >mark @set_device_version;
  device_event_class_id = gr >mark @set_device_event_class_id;
  name = gr >mark @set_name;
  severity = gr >mark @set_severity;

  ext_key = ( ascii - (sp|'='))+ >mark %set_k;
  ext_val = ( any+ ) >mark %set_v ;
  ext_pair = ( ext_key '=' ext_val ) >mark @set_ext;
  ext_fields  = ext_pair ( sp ext_pair )* ;

  main :=
'CEF' cln
version vl
device_vendor vl
device_product vl
device_version vl
device_event_class_id vl
name vl
severity vl
ext_fields vl
;

  write data;

  access m.;

  variable cs m.cs;
  variable p m.p;
  variable pe m.pe;
  variable eof m.eof;
  variable data m.data;

}%%


type CefEvent struct {
  // defaults to 0 which is also the first CEF version.
  Version            string
  DeviceVendor       string
  DeviceProduct      string
  DeviceVersion      string
  DeviceEventClassId string
  Name               string
  Severity           string
  Extensions
}

type Extensions         map[string]string

func NewCefEvent()  *CefEvent {
  return &CefEvent{
    Extensions: make(map[string]string,0),
  }
}


// Parse takes a slice of bytes as an input an fills Phone structure in case
// input is a phone number in one of valid formats.
func ParseCef(m *Machine, input []byte) (*CefEvent, error) {
  // Initialize variables required by Ragel.
  m.data = input
  m.pe = len(input)
  m.p = 0
  m.pb = 0
  m.err = nil
  m.eof = len(input)

  cef := NewCefEvent()

  var k,v string

  %% write init;
  %% write exec;

  if m.err != nil {
      return nil, m.err
  }

  return cef, m.err
}