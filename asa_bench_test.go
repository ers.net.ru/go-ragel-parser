package ragelparser

import (
	"testing"
)

//go:generate go test -bench=. -benchmem cef_bench_test.go

func BenchmarkParseASA(b *testing.B) {

	input := `<174>Dec 27 22:47:56 10.75.192.225 %ASA-6-302013: Built inbound TCP connection 2440678066 for zone-out:91.193.177.188/13823 (91.193.177.188/13823) to dmz23:10.72.229.177/443 (10.72.229.177/443)`

	for i := 0; i < b.N; i++ {
		ParseASA(input)
	}
}
