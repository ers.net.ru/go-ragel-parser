package ragelparser

import (
	"testing"
)

//go:generate go test -bench=. -benchmem cef_bench_test.go

func BenchmarkRagel(b *testing.B) {

	var (
		input = []byte(`CEF:0|Fortinet|FortiWeb|7.00|20000008|attack|alert|cat=Signature Detection act=Erase deviceExternalId=FV-3KETE21000002 deviceProcessName=srvpl-job.mts.ru sourceServiceName=default-triger-to-FAZ proto=tcp app=https/tls1.3 src=37.214.65.165 spt=11881 dst=10.74.60.33 dpt=443 requestMethod=get request=/undefined requestClientApplication=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6.1 Safari/605.1.15 dhost=job.mts.ru msg=HTTP Body triggered signature ID 080110001 of Signatures policy sp-job.mts.ru cn1=943922261 cn1Label=message ID cs1=root cs1Label=ADOM name cs2=High cs2Label=severity level cs3=Belarus cs3Label=source country cs4=Information Disclosure cs4Label=signature main class name cs5=IFrame Injection cs5Label=signature subclass name cs6=080110001 cs6Label=signature ID deviceCustomDate1=2022-11-09-19:36:02|`)
		m     = new(Machine)
	)

	for i := 0; i < b.N; i++ {
		ParseCef(m, input)
	}
}

func BenchmarkRegex(b *testing.B) {
	var (
		inputStr = `CEF:0|Fortinet|FortiWeb|7.00|20000008|attack|alert|cat=Signature Detection act=Erase deviceExternalId=FV-3KETE21000002 deviceProcessName=srvpl-job.mts.ru sourceServiceName=default-triger-to-FAZ proto=tcp app=https/tls1.3 src=37.214.65.165 spt=11881 dst=10.74.60.33 dpt=443 requestMethod=get request=/undefined requestClientApplication=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6.1 Safari/605.1.15 dhost=job.mts.ru msg=HTTP Body triggered signature ID 080110001 of Signatures policy sp-job.mts.ru cn1=943922261 cn1Label=message ID cs1=root cs1Label=ADOM name cs2=High cs2Label=severity level cs3=Belarus cs3Label=source country cs4=Information Disclosure cs4Label=signature main class name cs5=IFrame Injection cs5Label=signature subclass name cs6=080110001 cs6Label=signature ID deviceCustomDate1=2022-11-09-19:36:02`
	)

	for i := 0; i < b.N; i++ {
		CefStringToObject(inputStr)
	}
}

func BenchmarkKv(b *testing.B) {
	var (
		inputKv = []byte(`CEF:0|Fortinet|FortiWeb|7.00|20000008|attack|alert|cat=Signature Detection act=Erase deviceExternalId=FV-3KETE21000002 deviceProcessName=srvpl-job.mts.ru sourceServiceName=default-triger-to-FAZ proto=tcp app=https/tls1.3 src=37.214.65.165 spt=11881 dst=10.74.60.33 dpt=443 requestMethod=get request=/undefined requestClientApplication=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6.1 Safari/605.1.15 dhost=job.mts.ru msg=HTTP Body triggered signature ID 080110001 of Signatures policy sp-job.mts.ru cn1=943922261 cn1Label=message ID cs1=root cs1Label=ADOM name cs2=High cs2Label=severity level cs3=Belarus cs3Label=source country cs4=Information Disclosure cs4Label=signature main class name cs5=IFrame Injection cs5Label=signature subclass name cs6=080110001 cs6Label=signature ID deviceCustomDate1=2022-11-09-19:36:02`)
	)

	for i := 0; i < b.N; i++ {
		ParseKv(inputKv)
	}
}
