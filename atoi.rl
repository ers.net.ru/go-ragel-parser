// -*-go-*-
//
// Convert a string to an integer.
//
// To compile:
//
//   ragel -Z -T0 -o atoi.go atoi.rl
//   go build -o atoi atoi.go
//   ./atoi
//
// To show a diagram of your state machine:
//
//   ragel -V -Z -p -o atoi.dot atoi.rl
//   xdot atoi.dot
//

package ragelparser

import (
	"fmt"
)

%%{
	machine atoi;

	action see_neg   { neg = true }
	action add_digit { val = val * 10 + (int(fc) - '0') }

	main :=
	( '-'@see_neg | '+' )? ( digit @add_digit )+
	'\n'?
	;

	write data;

	access m.;

	variable cs m.cs;
	variable p m.p;
	variable pe m.pe;
	variable eof m.eof;
	variable data m.data;

}%%


func ParseAtoi(m *Machine, input []byte) (val int) {

	m.data = input
	m.pe = len(input)
	m.cs = 0
	m.p = 0
	m.pb = 0
	m.err = nil
	m.eof = len(input)

	neg := false

	%% write init;
	%% write exec;

	if neg {
		val = -1 * val;
	}

	if m.cs < atoi_first_final {
		fmt.Println("atoi: there was an error:", m.cs, "<", atoi_first_final)
		fmt.Println(input)
		for i := 0; i < m.p; i++ {
			fmt.Print(" ")
		}
		fmt.Println("^")
	}
	return val
}