
//line strdelim.go.rl:1
// -*-go-*-
//
// Convert a string to an integer.
//
// To compile:
//
//   ragel -Z -G2 -o strdelim.go strdelim.go.rl
//   go build -o strdelim strdelim.go
//   ./atoi
//
// To show a diagram of your state machine:
//
//   ragel -V -Z -p -o atoi.dot atoi.rl
//   xdot atoi.dot
//

package ragelparser

import (
	"strings"
)


//line strdelim.go:27
const strdelim_start int = 0
const strdelim_first_final int = 4
const strdelim_error int = -1

const strdelim_en_main int = 0


//line strdelim.go.rl:53



func ParseStrDelim(m *Machine, input []byte) (val [][]byte) {

	m.data = input
	m.pe = len(input)
	m.cs = 0
	m.p = 0
	m.pb = 0
	m.err = nil
	m.eof = len(input)

	val = make([][]byte,0)

	
//line strdelim.go:52
	{
	( m.cs) = strdelim_start
	}

//line strdelim.go.rl:69
	
//line strdelim.go:59
	{
	if ( m.p) == ( m.pe) {
		goto _test_eof
	}
	switch ( m.cs) {
	case 0:
		goto st_case_0
	case 1:
		goto st_case_1
	case 2:
		goto st_case_2
	case 3:
		goto st_case_3
	case 4:
		goto st_case_4
	case 5:
		goto st_case_5
	case 6:
		goto st_case_6
	}
	goto st_out
	st_case_0:
		goto tr0
tr0:
//line strdelim.go.rl:26
 m.pb = m.p 
	goto st1
	st1:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof1
		}
	st_case_1:
//line strdelim.go:92
		if ( m.data)[( m.p)] == 92 {
			goto tr2
		}
		goto st1
tr2:
//line strdelim.go.rl:35

		val = append(val,m.data[m.pb:m.p])
    
	goto st2
	st2:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof2
		}
	st_case_2:
//line strdelim.go:108
		switch ( m.data)[( m.p)] {
		case 32:
			goto st3
		case 92:
			goto tr2
		case 124:
			goto st3
		}
		goto st1
	st3:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof3
		}
	st_case_3:
		if ( m.data)[( m.p)] == 92 {
			goto tr5
		}
		goto tr4
tr4:
//line strdelim.go.rl:26
 m.pb = m.p 
	goto st4
	st4:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof4
		}
	st_case_4:
//line strdelim.go:136
		if ( m.data)[( m.p)] == 92 {
			goto tr7
		}
		goto st4
tr7:
//line strdelim.go.rl:35

		val = append(val,m.data[m.pb:m.p])
    
	goto st5
tr5:
//line strdelim.go.rl:35

		val = append(val,m.data[m.pb:m.p])
    
//line strdelim.go.rl:26
 m.pb = m.p 
	goto st5
	st5:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof5
		}
	st_case_5:
//line strdelim.go:160
		switch ( m.data)[( m.p)] {
		case 32:
			goto st6
		case 92:
			goto tr7
		case 124:
			goto st6
		}
		goto st4
	st6:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof6
		}
	st_case_6:
		if ( m.data)[( m.p)] == 92 {
			goto tr5
		}
		goto tr4
	st_out:
	_test_eof1: ( m.cs) = 1; goto _test_eof
	_test_eof2: ( m.cs) = 2; goto _test_eof
	_test_eof3: ( m.cs) = 3; goto _test_eof
	_test_eof4: ( m.cs) = 4; goto _test_eof
	_test_eof5: ( m.cs) = 5; goto _test_eof
	_test_eof6: ( m.cs) = 6; goto _test_eof

	_test_eof: {}
	if ( m.p) == ( m.eof) {
		switch ( m.cs) {
		case 4, 5, 6:
//line strdelim.go.rl:35

		val = append(val,m.data[m.pb:m.p])
    
//line strdelim.go:195
		}
	}

	}

//line strdelim.go.rl:70

	return val
}

func ssToString(in [][]byte) string {

	var b strings.Builder

	for i := 0; i < len(in); i++ {
		b.Write(in[i])
	}

	return b.String()
}