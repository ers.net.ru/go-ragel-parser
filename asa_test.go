package ragelparser

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseASA(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  map[string]string
	}{
		{
			name:  "1",
			input: "<174>Dec 27 22:48:00 10.75.192.225 %ASA-6-302021: Teardown ICMP connection for faddr 213.87.2.248/52834 gaddr 10.72.224.203/0 laddr 10.72.224.203/0 type 8 code 0",
			want: map[string]string{
				"d": "sdf",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, ParseASA(tt.input), "ParseASA(%v)", tt.input)
		})
	}
}
