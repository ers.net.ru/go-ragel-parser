package ragelparser

// Machine contains variables used by Ragel auto generated code.
// See Ragel docs for details.
type Machine struct {
	// Data to process.
	data []byte
	// Data pointer.
	p int
	// Data end pointer.
	pe int
	// Curent state.
	cs int
	// End of file pointer.
	eof int
	// Start of current date block.
	pb int
	// Current err
	err error
}

// New initialized new Machine structure.
func New() *Machine {
	return &Machine{}
}

// string returns current parsed variable. Variable m.pb should be updated by
// "mark" action, while m.p is a current parser position inside m.data.
func (m *Machine) string() string {
	return string(m.data[m.pb:m.p])
}

func (m *Machine) bytes() []byte {
	return m.data[m.pb:m.p]
}
