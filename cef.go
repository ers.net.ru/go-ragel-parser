//line cef.go.rl:1
package ragelparser

//go:generate ragel -Z -G2 -o cef.go cef.go.rl

//line cef.go:9
const cef_start int = 1
const cef_first_final int = 26
const cef_error int = 0

const cef_en_main int = 1

//line cef.go.rl:98

type CefEvent struct {
	// defaults to 0 which is also the first CEF version.
	Version            string
	DeviceVendor       string
	DeviceProduct      string
	DeviceVersion      string
	DeviceEventClassId string
	Name               string
	Severity           string
	Extensions
}

type Extensions map[string]string

func NewCefEvent() *CefEvent {
	return &CefEvent{
		Extensions: make(map[string]string, 0),
	}
}

// Parse takes a slice of bytes as an input an fills Phone structure in case
// input is a phone number in one of valid formats.
func ParseCef(m *Machine, input []byte) (*CefEvent, error) {
	// Initialize variables required by Ragel.
	m.data = input
	m.pe = len(input)
	m.p = 0
	m.pb = 0
	m.err = nil
	m.eof = len(input)

	cef := NewCefEvent()

	var k, v string

	//line cef.go:58
	{
		(m.cs) = cef_start
	}

	//line cef.go.rl:138

	//line cef.go:65
	{
		if (m.p) == (m.pe) {
			goto _test_eof
		}
		switch m.cs {
		case 1:
			goto st_case_1
		case 0:
			goto st_case_0
		case 2:
			goto st_case_2
		case 3:
			goto st_case_3
		case 4:
			goto st_case_4
		case 5:
			goto st_case_5
		case 6:
			goto st_case_6
		case 7:
			goto st_case_7
		case 8:
			goto st_case_8
		case 9:
			goto st_case_9
		case 10:
			goto st_case_10
		case 11:
			goto st_case_11
		case 12:
			goto st_case_12
		case 13:
			goto st_case_13
		case 14:
			goto st_case_14
		case 15:
			goto st_case_15
		case 16:
			goto st_case_16
		case 17:
			goto st_case_17
		case 18:
			goto st_case_18
		case 19:
			goto st_case_19
		case 20:
			goto st_case_20
		case 21:
			goto st_case_21
		case 22:
			goto st_case_22
		case 23:
			goto st_case_23
		case 24:
			goto st_case_24
		case 25:
			goto st_case_25
		case 26:
			goto st_case_26
		case 27:
			goto st_case_27
		}
		goto st_out
	st_case_1:
		if (m.data)[(m.p)] == 67 {
			goto st2
		}
		goto st0
	st_case_0:
	st0:
		(m.cs) = 0
		goto _out
	st2:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof2
		}
	st_case_2:
		if (m.data)[(m.p)] == 69 {
			goto st3
		}
		goto st0
	st3:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof3
		}
	st_case_3:
		if (m.data)[(m.p)] == 70 {
			goto st4
		}
		goto st0
	st4:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof4
		}
	st_case_4:
		if (m.data)[(m.p)] == 58 {
			goto st5
		}
		goto st0
	st5:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof5
		}
	st_case_5:
		if 48 <= (m.data)[(m.p)] && (m.data)[(m.p)] <= 57 {
			goto tr5
		}
		goto st0
	tr5:
		//line cef.go.rl:10

		m.pb = m.p

		//line cef.go.rl:15

		cef.Version = string(m.data[m.pb : m.p+1])

		goto st6
	st6:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof6
		}
	st_case_6:
		//line cef.go:189
		if (m.data)[(m.p)] == 124 {
			goto st7
		}
		goto st0
	st7:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof7
		}
	st_case_7:
		if (m.data)[(m.p)] == 124 {
			goto st0
		}
		goto tr7
	tr7:
		//line cef.go.rl:10

		m.pb = m.p

		//line cef.go.rl:19

		cef.DeviceVendor = string(m.data[m.pb : m.p+1])

		goto st8
	tr8:
		//line cef.go.rl:19

		cef.DeviceVendor = string(m.data[m.pb : m.p+1])

		goto st8
	st8:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof8
		}
	st_case_8:
		//line cef.go:224
		if (m.data)[(m.p)] == 124 {
			goto st9
		}
		goto tr8
	st9:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof9
		}
	st_case_9:
		if (m.data)[(m.p)] == 124 {
			goto st0
		}
		goto tr10
	tr10:
		//line cef.go.rl:10

		m.pb = m.p

		//line cef.go.rl:23

		cef.DeviceProduct = string(m.data[m.pb : m.p+1])

		goto st10
	tr11:
		//line cef.go.rl:23

		cef.DeviceProduct = string(m.data[m.pb : m.p+1])

		goto st10
	st10:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof10
		}
	st_case_10:
		//line cef.go:259
		if (m.data)[(m.p)] == 124 {
			goto st11
		}
		goto tr11
	st11:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof11
		}
	st_case_11:
		if (m.data)[(m.p)] == 124 {
			goto st0
		}
		goto tr13
	tr13:
		//line cef.go.rl:10

		m.pb = m.p

		//line cef.go.rl:27

		cef.DeviceVersion = string(m.data[m.pb : m.p+1])

		goto st12
	tr14:
		//line cef.go.rl:27

		cef.DeviceVersion = string(m.data[m.pb : m.p+1])

		goto st12
	st12:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof12
		}
	st_case_12:
		//line cef.go:294
		if (m.data)[(m.p)] == 124 {
			goto st13
		}
		goto tr14
	st13:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof13
		}
	st_case_13:
		if (m.data)[(m.p)] == 124 {
			goto st0
		}
		goto tr16
	tr16:
		//line cef.go.rl:10

		m.pb = m.p

		//line cef.go.rl:31

		cef.DeviceEventClassId = string(m.data[m.pb : m.p+1])

		goto st14
	tr17:
		//line cef.go.rl:31

		cef.DeviceEventClassId = string(m.data[m.pb : m.p+1])

		goto st14
	st14:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof14
		}
	st_case_14:
		//line cef.go:329
		if (m.data)[(m.p)] == 124 {
			goto st15
		}
		goto tr17
	st15:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof15
		}
	st_case_15:
		if (m.data)[(m.p)] == 124 {
			goto st0
		}
		goto tr19
	tr19:
		//line cef.go.rl:10

		m.pb = m.p

		//line cef.go.rl:35

		cef.Name = string(m.data[m.pb : m.p+1])

		goto st16
	tr20:
		//line cef.go.rl:35

		cef.Name = string(m.data[m.pb : m.p+1])

		goto st16
	st16:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof16
		}
	st_case_16:
		//line cef.go:364
		if (m.data)[(m.p)] == 124 {
			goto st17
		}
		goto tr20
	st17:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof17
		}
	st_case_17:
		if (m.data)[(m.p)] == 124 {
			goto st0
		}
		goto tr22
	tr22:
		//line cef.go.rl:10

		m.pb = m.p

		//line cef.go.rl:39

		cef.Severity = string(m.data[m.pb : m.p+1])

		goto st18
	tr23:
		//line cef.go.rl:39

		cef.Severity = string(m.data[m.pb : m.p+1])

		goto st18
	st18:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof18
		}
	st_case_18:
		//line cef.go:399
		if (m.data)[(m.p)] == 124 {
			goto st19
		}
		goto tr23
	st19:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof19
		}
	st_case_19:
		switch {
		case (m.data)[(m.p)] < 33:
			if (m.data)[(m.p)] <= 31 {
				goto tr25
			}
		case (m.data)[(m.p)] > 60:
			if 62 <= (m.data)[(m.p)] && (m.data)[(m.p)] <= 127 {
				goto tr25
			}
		default:
			goto tr25
		}
		goto st0
	tr25:
		//line cef.go.rl:10

		m.pb = m.p

		goto st20
	st20:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof20
		}
	st_case_20:
		//line cef.go:433
		if (m.data)[(m.p)] == 61 {
			goto tr27
		}
		switch {
		case (m.data)[(m.p)] > 31:
			if 33 <= (m.data)[(m.p)] && (m.data)[(m.p)] <= 127 {
				goto st20
			}
		default:
			goto st20
		}
		goto st0
	tr27:
		//line cef.go.rl:43

		k = m.string()

		goto st21
	st21:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof21
		}
	st_case_21:
		//line cef.go:457
		goto tr28
	tr28:
		//line cef.go.rl:10

		m.pb = m.p

		//line cef.go.rl:51

		cef.Extensions[k] = v

		goto st22
	tr29:
		//line cef.go.rl:51

		cef.Extensions[k] = v

		goto st22
	tr37:
		//line cef.go.rl:51

		cef.Extensions[k] = v

		//line cef.go.rl:10

		m.pb = m.p

		goto st22
	st22:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof22
		}
	st_case_22:
		//line cef.go:493
		switch (m.data)[(m.p)] {
		case 32:
			goto tr30
		case 124:
			goto tr31
		}
		goto tr29
	tr30:
		//line cef.go.rl:47

		v = m.string()

		//line cef.go.rl:51

		cef.Extensions[k] = v

		goto st23
	tr38:
		//line cef.go.rl:47

		v = m.string()

		//line cef.go.rl:51

		cef.Extensions[k] = v

		//line cef.go.rl:10

		m.pb = m.p

		goto st23
	st23:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof23
		}
	st_case_23:
		//line cef.go:532
		switch (m.data)[(m.p)] {
		case 32:
			goto tr30
		case 124:
			goto tr33
		}
		switch {
		case (m.data)[(m.p)] > 60:
			if 62 <= (m.data)[(m.p)] && (m.data)[(m.p)] <= 127 {
				goto tr32
			}
		default:
			goto tr32
		}
		goto tr29
	tr34:
		//line cef.go.rl:51

		cef.Extensions[k] = v

		goto st24
	tr32:
		//line cef.go.rl:51

		cef.Extensions[k] = v

		//line cef.go.rl:10

		m.pb = m.p

		goto st24
	st24:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof24
		}
	st_case_24:
		//line cef.go:571
		switch (m.data)[(m.p)] {
		case 32:
			goto tr30
		case 61:
			goto tr35
		case 124:
			goto tr36
		}
		if (m.data)[(m.p)] <= 127 {
			goto tr34
		}
		goto tr29
	tr35:
		//line cef.go.rl:51

		cef.Extensions[k] = v

		//line cef.go.rl:43

		k = m.string()

		goto st25
	st25:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof25
		}
	st_case_25:
		//line cef.go:600
		switch (m.data)[(m.p)] {
		case 32:
			goto tr38
		case 124:
			goto tr39
		}
		goto tr37
	tr31:
		//line cef.go.rl:47

		v = m.string()

		//line cef.go.rl:51

		cef.Extensions[k] = v

		goto st26
	tr39:
		//line cef.go.rl:47

		v = m.string()

		//line cef.go.rl:51

		cef.Extensions[k] = v

		//line cef.go.rl:10

		m.pb = m.p

		goto st26
	st26:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof26
		}
	st_case_26:
		//line cef.go:639
		switch (m.data)[(m.p)] {
		case 32:
			goto tr30
		case 124:
			goto tr31
		}
		goto tr29
	tr36:
		//line cef.go.rl:47

		v = m.string()

		//line cef.go.rl:51

		cef.Extensions[k] = v

		goto st27
	tr33:
		//line cef.go.rl:47

		v = m.string()

		//line cef.go.rl:51

		cef.Extensions[k] = v

		//line cef.go.rl:10

		m.pb = m.p

		goto st27
	st27:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof27
		}
	st_case_27:
		//line cef.go:678
		switch (m.data)[(m.p)] {
		case 32:
			goto tr30
		case 61:
			goto tr35
		case 124:
			goto tr36
		}
		if (m.data)[(m.p)] <= 127 {
			goto tr34
		}
		goto tr29
	st_out:
	_test_eof2:
		(m.cs) = 2
		goto _test_eof
	_test_eof3:
		(m.cs) = 3
		goto _test_eof
	_test_eof4:
		(m.cs) = 4
		goto _test_eof
	_test_eof5:
		(m.cs) = 5
		goto _test_eof
	_test_eof6:
		(m.cs) = 6
		goto _test_eof
	_test_eof7:
		(m.cs) = 7
		goto _test_eof
	_test_eof8:
		(m.cs) = 8
		goto _test_eof
	_test_eof9:
		(m.cs) = 9
		goto _test_eof
	_test_eof10:
		(m.cs) = 10
		goto _test_eof
	_test_eof11:
		(m.cs) = 11
		goto _test_eof
	_test_eof12:
		(m.cs) = 12
		goto _test_eof
	_test_eof13:
		(m.cs) = 13
		goto _test_eof
	_test_eof14:
		(m.cs) = 14
		goto _test_eof
	_test_eof15:
		(m.cs) = 15
		goto _test_eof
	_test_eof16:
		(m.cs) = 16
		goto _test_eof
	_test_eof17:
		(m.cs) = 17
		goto _test_eof
	_test_eof18:
		(m.cs) = 18
		goto _test_eof
	_test_eof19:
		(m.cs) = 19
		goto _test_eof
	_test_eof20:
		(m.cs) = 20
		goto _test_eof
	_test_eof21:
		(m.cs) = 21
		goto _test_eof
	_test_eof22:
		(m.cs) = 22
		goto _test_eof
	_test_eof23:
		(m.cs) = 23
		goto _test_eof
	_test_eof24:
		(m.cs) = 24
		goto _test_eof
	_test_eof25:
		(m.cs) = 25
		goto _test_eof
	_test_eof26:
		(m.cs) = 26
		goto _test_eof
	_test_eof27:
		(m.cs) = 27
		goto _test_eof

	_test_eof:
		{
		}
	_out:
		{
		}
	}

	//line cef.go.rl:139

	if m.err != nil {
		return nil, m.err
	}

	return cef, m.err
}
