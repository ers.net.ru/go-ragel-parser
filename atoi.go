//line atoi.rl:1
// -*-go-*-
//
// Convert a string to an integer.
//
// To compile:
//
//   ragel -Z -T0 -o atoi.go atoi.rl
//   go build -o atoi atoi.go
//   ./atoi
//
// To show a diagram of your state machine:
//
//   ragel -V -Z -p -o atoi.dot atoi.rl
//   xdot atoi.dot
//

package ragelparser

import (
	"fmt"
)

//line atoi.go:27
const atoi_start int = 1
const atoi_first_final int = 3
const atoi_error int = 0

const atoi_en_main int = 1

//line atoi.rl:44

func ParseAtoi(m *Machine, input []byte) (val int) {

	m.data = input
	m.pe = len(input)
	m.cs = 0
	m.p = 0
	m.pb = 0
	m.err = nil
	m.eof = len(input)

	neg := false

	//line atoi.go:52
	{
		(m.cs) = atoi_start
	}

	//line atoi.rl:60

	//line atoi.go:59
	{
		if (m.p) == (m.pe) {
			goto _test_eof
		}
		switch m.cs {
		case 1:
			goto st_case_1
		case 0:
			goto st_case_0
		case 2:
			goto st_case_2
		case 3:
			goto st_case_3
		case 4:
			goto st_case_4
		}
		goto st_out
	st_case_1:
		switch (m.data)[(m.p)] {
		case 43:
			goto st2
		case 45:
			goto tr2
		}
		if 48 <= (m.data)[(m.p)] && (m.data)[(m.p)] <= 57 {
			goto tr3
		}
		goto st0
	st_case_0:
	st0:
		(m.cs) = 0
		goto _out
	tr2:
		//line atoi.rl:26
		neg = true
		goto st2
	st2:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof2
		}
	st_case_2:
		//line atoi.go:101
		if 48 <= (m.data)[(m.p)] && (m.data)[(m.p)] <= 57 {
			goto tr3
		}
		goto st0
	tr3:
		//line atoi.rl:27
		val = val*10 + (int((m.data)[(m.p)]) - '0')
		goto st3
	st3:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof3
		}
	st_case_3:
		//line atoi.go:115
		if (m.data)[(m.p)] == 10 {
			goto st4
		}
		if 48 <= (m.data)[(m.p)] && (m.data)[(m.p)] <= 57 {
			goto tr3
		}
		goto st0
	st4:
		if (m.p)++; (m.p) == (m.pe) {
			goto _test_eof4
		}
	st_case_4:
		goto st0
	st_out:
	_test_eof2:
		(m.cs) = 2
		goto _test_eof
	_test_eof3:
		(m.cs) = 3
		goto _test_eof
	_test_eof4:
		(m.cs) = 4
		goto _test_eof

	_test_eof:
		{
		}
	_out:
		{
		}
	}

	//line atoi.rl:61

	if neg {
		val = -1 * val
	}

	if m.cs < atoi_first_final {
		fmt.Println("atoi: there was an error:", m.cs, "<", atoi_first_final)
		fmt.Println(input)
		for i := 0; i < m.p; i++ {
			fmt.Print(" ")
		}
		fmt.Println("^")
	}

	return val
}
