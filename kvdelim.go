
//line kvdelim.go.rl:1
// To compile:
//go:generate ragel -Z -G2 -o kvdelim.go kvdelim.go.rl

// To show a diagram of your state machine:
//go:generate ragel -V -Z -p -o kvdelim.dot kvdelim.go.rl

//To show a diagram of your state machine:
//go:generate dot -Tsvg kvdelim.dot -o kvdelim.svg

package ragelparser

import "strings"


//line kvdelim.go:18
const kvdelim_start int = 1
const kvdelim_first_final int = 3
const kvdelim_error int = 0

const kvdelim_en_main int = 1


//line kvdelim.go.rl:74



func ParseKvDelim(m *Machine, input []byte) (val map[string]string) {

	m.data = input
	m.pe = len(input)
	m.cs = 0
	m.p = 0
	m.pb = 0
	m.err = nil
	m.eof = len(input)

	k := strings.Builder{}
	v := strings.Builder{}

	val = make(map[string]string,0)

	
//line kvdelim.go:46
	{
	( m.cs) = kvdelim_start
	}

//line kvdelim.go.rl:93
	
//line kvdelim.go:53
	{
	if ( m.p) == ( m.pe) {
		goto _test_eof
	}
	switch ( m.cs) {
	case 1:
		goto st_case_1
	case 0:
		goto st_case_0
	case 2:
		goto st_case_2
	case 3:
		goto st_case_3
	case 4:
		goto st_case_4
	case 5:
		goto st_case_5
	case 6:
		goto st_case_6
	case 7:
		goto st_case_7
	case 8:
		goto st_case_8
	case 9:
		goto st_case_9
	case 10:
		goto st_case_10
	}
	goto st_out
	st_case_1:
		switch {
		case ( m.data)[( m.p)] < 65:
			if 48 <= ( m.data)[( m.p)] && ( m.data)[( m.p)] <= 57 {
				goto tr0
			}
		case ( m.data)[( m.p)] > 90:
			if 97 <= ( m.data)[( m.p)] && ( m.data)[( m.p)] <= 122 {
				goto tr0
			}
		default:
			goto tr0
		}
		goto st0
st_case_0:
	st0:
		( m.cs) = 0
		goto _out
tr0:
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st2
	st2:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof2
		}
	st_case_2:
//line kvdelim.go:110
		if ( m.data)[( m.p)] == 61 {
			goto tr3
		}
		switch {
		case ( m.data)[( m.p)] < 65:
			if 48 <= ( m.data)[( m.p)] && ( m.data)[( m.p)] <= 57 {
				goto st2
			}
		case ( m.data)[( m.p)] > 90:
			if 97 <= ( m.data)[( m.p)] && ( m.data)[( m.p)] <= 122 {
				goto st2
			}
		default:
			goto st2
		}
		goto st0
tr3:
//line kvdelim.go.rl:19

		k.Write(m.data[m.pb:m.p])
    
	goto st3
	st3:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof3
		}
	st_case_3:
//line kvdelim.go:138
		switch ( m.data)[( m.p)] {
		case 32:
			goto tr5
		case 92:
			goto tr6
		}
		goto tr4
tr4:
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st4
tr16:
//line kvdelim.go.rl:27

		v.Write(m.data[m.pb+1:m.p])
	
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st4
	st4:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof4
		}
	st_case_4:
//line kvdelim.go:163
		switch ( m.data)[( m.p)] {
		case 32:
			goto tr8
		case 92:
			goto tr9
		}
		goto st4
tr5:
//line kvdelim.go.rl:17
 m.pb = m.p 
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:31

		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	
	goto st5
tr8:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:31

		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	
	goto st5
tr13:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:31

		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st5
tr17:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:27

		v.Write(m.data[m.pb+1:m.p])
	
//line kvdelim.go.rl:17
 m.pb = m.p 
//line kvdelim.go.rl:31

		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	
	goto st5
	st5:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof5
		}
	st_case_5:
//line kvdelim.go:238
		switch ( m.data)[( m.p)] {
		case 32:
			goto tr8
		case 92:
			goto tr9
		}
		switch {
		case ( m.data)[( m.p)] < 65:
			if 48 <= ( m.data)[( m.p)] && ( m.data)[( m.p)] <= 57 {
				goto tr10
			}
		case ( m.data)[( m.p)] > 90:
			if 97 <= ( m.data)[( m.p)] && ( m.data)[( m.p)] <= 122 {
				goto tr10
			}
		default:
			goto tr10
		}
		goto st4
tr10:
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st6
	st6:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof6
		}
	st_case_6:
//line kvdelim.go:267
		switch ( m.data)[( m.p)] {
		case 32:
			goto tr8
		case 61:
			goto tr12
		case 92:
			goto tr9
		}
		switch {
		case ( m.data)[( m.p)] < 65:
			if 48 <= ( m.data)[( m.p)] && ( m.data)[( m.p)] <= 57 {
				goto st6
			}
		case ( m.data)[( m.p)] > 90:
			if 97 <= ( m.data)[( m.p)] && ( m.data)[( m.p)] <= 122 {
				goto st6
			}
		default:
			goto st6
		}
		goto st4
tr12:
//line kvdelim.go.rl:19

		k.Write(m.data[m.pb:m.p])
    
	goto st7
	st7:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof7
		}
	st_case_7:
//line kvdelim.go:300
		switch ( m.data)[( m.p)] {
		case 32:
			goto tr13
		case 92:
			goto tr9
		}
		goto tr4
tr6:
//line kvdelim.go.rl:17
 m.pb = m.p 
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
	goto st8
tr9:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st8
tr18:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:27

		v.Write(m.data[m.pb+1:m.p])
	
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st8
	st8:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof8
		}
	st_case_8:
//line kvdelim.go:341
		switch ( m.data)[( m.p)] {
		case 32:
			goto tr8
		case 61:
			goto st9
		case 92:
			goto tr15
		case 124:
			goto st9
		}
		goto st4
tr19:
//line kvdelim.go.rl:27

		v.Write(m.data[m.pb+1:m.p])
	
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st9
	st9:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof9
		}
	st_case_9:
//line kvdelim.go:366
		switch ( m.data)[( m.p)] {
		case 32:
			goto tr17
		case 92:
			goto tr18
		}
		goto tr16
tr15:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st10
tr20:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:27

		v.Write(m.data[m.pb+1:m.p])
	
//line kvdelim.go.rl:17
 m.pb = m.p 
	goto st10
	st10:
		if ( m.p)++; ( m.p) == ( m.pe) {
			goto _test_eof10
		}
	st_case_10:
//line kvdelim.go:399
		switch ( m.data)[( m.p)] {
		case 32:
			goto tr17
		case 61:
			goto tr19
		case 92:
			goto tr20
		case 124:
			goto tr19
		}
		goto tr16
	st_out:
	_test_eof2: ( m.cs) = 2; goto _test_eof
	_test_eof3: ( m.cs) = 3; goto _test_eof
	_test_eof4: ( m.cs) = 4; goto _test_eof
	_test_eof5: ( m.cs) = 5; goto _test_eof
	_test_eof6: ( m.cs) = 6; goto _test_eof
	_test_eof7: ( m.cs) = 7; goto _test_eof
	_test_eof8: ( m.cs) = 8; goto _test_eof
	_test_eof9: ( m.cs) = 9; goto _test_eof
	_test_eof10: ( m.cs) = 10; goto _test_eof

	_test_eof: {}
	if ( m.p) == ( m.eof) {
		switch ( m.cs) {
		case 4, 5, 6, 8:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:31

		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	
		case 3:
//line kvdelim.go.rl:17
 m.pb = m.p 
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:31

		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	
		case 7:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:31

		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	
//line kvdelim.go.rl:17
 m.pb = m.p 
		case 9, 10:
//line kvdelim.go.rl:23

		v.Write(m.data[m.pb:m.p])
    
//line kvdelim.go.rl:27

		v.Write(m.data[m.pb+1:m.p])
	
//line kvdelim.go.rl:17
 m.pb = m.p 
//line kvdelim.go.rl:31

		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	
//line kvdelim.go:483
		}
	}

	_out: {}
	}

//line kvdelim.go.rl:94

	return val
}