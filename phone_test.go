package ragelparser

import (
	"reflect"
	"testing"
)

func TestMachine_Parse(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name    string
		args    args
		want    *Phone
		wantErr bool
	}{
		{
			name: "1",
			args: args{
				input: []byte("+1 (555) 2334567"),
			},
			want: &Phone{
				IntCode:  "1",
				AreaCode: "555",
				Number:   "2334567",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := New()
			got, err := ParsePhone(m, tt.args.input)

			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
