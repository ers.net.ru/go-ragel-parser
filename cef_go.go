package ragelparser

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/dlclark/regexp2"
	"github.com/jjeffery/kv"
)

// copied and modified from
// https://github.com/rfizzle/log-collector/blob/master/clients/syslog/parser/cef.go

var extensionMapping map[string]string = map[string]string{
	"act":                          "DeviceAction",
	"app":                          "ApplicationProtocol",
	"c6a1":                         "DeviceCustomIPv6Address1",
	"c6a1Label":                    "DeviceCustomIPv6Address1Label",
	"c6a2":                         "DeviceCustomIPv6Address2",
	"c6a2Label":                    "DeviceCustomIPv6Address2Label",
	"c6a3":                         "DeviceCustomIPv6Address3",
	"c6a3Label":                    "DeviceCustomIPv6Address3Label",
	"c6a4":                         "DeviceCustomIPv6Address4",
	"c6a4Label":                    "DeviceCustomIPv6Address4Label",
	"cat":                          "DeviceEventCategory",
	"cfp1":                         "DeviceCustomFloatingPoint1",
	"cfp1Label":                    "DeviceCustomFloatingPoint1Label",
	"cfp2":                         "DeviceCustomFloatingPoint2",
	"cfp2Label":                    "DeviceCustomFloatingPoint2Label",
	"cfp3":                         "DeviceCustomFloatingPoint3",
	"cfp3Label":                    "DeviceCustomFloatingPoint3Label",
	"cfp4":                         "DeviceCustomFloatingPoint4",
	"cfp4Label":                    "DeviceCustomFloatingPoint4Label",
	"cn1":                          "DeviceCustomNumber1",
	"cn1Label":                     "DeviceCustomNumber1Label",
	"cn2":                          "DeviceCustomNumber2",
	"cn2Label":                     "DeviceCustomNumber2Label",
	"cn3":                          "DeviceCustomNumber3",
	"cn3Label":                     "DeviceCustomNumber3Label",
	"cnt":                          "BaseEventCount",
	"cs1":                          "DeviceCustomString1",
	"cs1Label":                     "DeviceCustomString1Label",
	"cs2":                          "DeviceCustomString2",
	"cs2Label":                     "DeviceCustomString2Label",
	"cs3":                          "DeviceCustomString3",
	"cs3Label":                     "DeviceCustomString3Label",
	"cs4":                          "DeviceCustomString4",
	"cs4Label":                     "DeviceCustomString4Label",
	"cs5":                          "DeviceCustomString5",
	"cs5Label":                     "DeviceCustomString5Label",
	"cs6":                          "DeviceCustomString6",
	"cs6Label":                     "DeviceCustomString6Label",
	"destinationDnsDomain":         "DestinationDnsDomain",
	"destinationServiceName":       "DestinationServiceName",
	"destinationTranslatedAddress": "DestinationTranslatedAddress",
	"destinationTranslatedPort":    "DestinationTranslatedPort",
	"deviceCustomDate1":            "DeviceCustomDate1",
	"deviceCustomDate1Label":       "DeviceCustomDate1Label",
	"deviceCustomDate2":            "DeviceCustomDate2",
	"deviceCustomDate2Label":       "DeviceCustomDate2Label",
	"deviceDirection":              "DeviceDirection",
	"deviceDnsDomain":              "DeviceDnsDomain",
	"deviceExternalId":             "DeviceExternalId",
	"deviceFacility":               "DeviceFacility",
	"deviceInboundInterface":       "DeviceInboundInterface",
	"deviceNtDomain":               "DeviceNtDomain",
	"deviceOutboundInterface":      "DeviceOutboundInterface",
	"devicePayloadId":              "DevicePayloadId",
	"deviceProcessName":            "DeviceProcessName",
	"deviceTranslatedAddress":      "DeviceTranslatedAddress",
	"dhost":                        "DestinationHostName",
	"dmac":                         "DeviceMacAddress",
	"dntdom":                       "DestinationNtDomain",
	"dpid":                         "DestinationProcessId",
	"dpriv":                        "DestinationUserPrivileges",
	"dproc":                        "DestinationProcessName",
	"dpt":                          "DestinationPort",
	"dst":                          "DestinationAddress",
	"dtz":                          "DeviceTimeZone",
	"duid":                         "DestinationUserId",
	"duser":                        "DestinationUserName",
	"dvc":                          "DeviceAddress",
	"dvchost":                      "DeviceHostName",
	"dvcpid":                       "DeviceProcessId",
	"end":                          "EndTime",
	"externalId":                   "ExternalId",
	"fileCreateTime":               "FileCreateTime",
	"fileHash":                     "FileHash",
	"fileId":                       "FileId",
	"fileModificationTime":         "FileModificationTime",
	"filePath":                     "FilePath",
	"filePermission":               "FilePermission",
	"fileType":                     "FileType",
	"flexDate1":                    "FlexDate1",
	"flexDate1Label":               "FlexDate1Label",
	"flexString1":                  "FlexString1",
	"flexString1Label":             "FlexString1Label",
	"flexString2":                  "FlexString2",
	"flexString2Label":             "FlexString2Label",
	"fname":                        "Filename",
	"fsize":                        "FileSize",
	"in":                           "BytesIn",
	"msg":                          "Message",
	"oldFileCreateTime":            "OldFileCreateTime",
	"oldFileHash":                  "OldFileHash",
	"oldFileId":                    "OldFileId",
	"oldFileModificationTime":      "OldFileModificationTime",
	"oldFileName":                  "OldFileName",
	"oldFilePath":                  "OldFilePath",
	"oldFilePermission":            "OldFilePermission",
	"oldFileSize":                  "OldFileSize",
	"oldFileType":                  "OldFileType",
	"out":                          "BytesOut",
	"outcome":                      "EventOutcome",
	"proto":                        "TransportProtocol",
	"reason":                       "Reason",
	"request":                      "RequestUrl",
	"requestClientApplication":     "RequestClientApplication",
	"requestContext":               "RequestContext",
	"requestCookies":               "RequestCookies",
	"requestMethod":                "RequestMethod",
	"rt":                           "DeviceReceiptTime",
	"shost":                        "SourceHostName",
	"smac":                         "SourceMacAddress",
	"sntdom":                       "SourceNtDomain",
	"sourceDnsDomain":              "SourceDnsDomain",
	"sourceServiceName":            "SourceServiceName",
	"sourceTranslatedAddress":      "SourceTranslatedAddress",
	"sourceTranslatedPort":         "SourceTranslatedPort",
	"spid":                         "SourceProcessId",
	"spriv":                        "SourceUserPrivileges",
	"sproc":                        "SourceProcessName",
	"spt":                          "SourcePort",
	"src":                          "SourceAddress",
	"start":                        "StartTime",
	"suid":                         "SourceUserId",
	"suser":                        "SourceUserName",
	"type":                         "Type",
	"agentTranslatedZoneKey":       "AgentTranslatedZoneKey",
	"agentZoneKey":                 "AgentZoneKey",
	"customerKey":                  "CustomerKey",
	"dTranslatedZoneKey":           "DTranslatedZoneKey",
	"dZoneKey":                     "DZoneKey",
	"deviceTranslatedZoneKey":      "DeviceTranslatedZoneKey",
	"deviceZoneKey":                "DeviceZoneKey",
	"sTranslatedZoneKey":           "STranslatedZoneKey",
	"sZoneKey":                     "SZoneKey",
	"reportedDuration":             "ReportedDuration",
	"reportedResourceGroupName":    "ReportedResourceGroupName",
	"reportedResourceID":           "ReportedResourceID",
	"reportedResourceName":         "ReportedResourceName",
	"reportedResourceType":         "ReportedResourceType",
	"frameworkName":                "FrameworkName",
	"threatActor":                  "ThreatActor",
	"threatAttackID":               "ThreatAttackID",
}

type (
	CefExtra      map[string]string
	CefExtensions map[string]string
)

//type CefEvent struct {
//	Version            string   `json:"CEFVersion"`
//	DeviceVendor       string   `json:"DeviceVendor"`
//	DeviceProduct      string   `json:"DeviceProduct"`
//	DeviceVersion      string   `json:"DeviceVersion"`
//	DeviceEventClassId string   `json:"DeviceEventClassId"`
//	Name               string   `json:"Name"`
//	Severity           string   `json:"Severity"`
//	Extra              CefExtra `json:"Extra"`
//	CefExtensions      `yaml:",inline"`
//}

func ParseCefNative(event string) ([]byte, error) {
	cefEvent, err := CefStringToObject(event)
	if err != nil {
		return nil, err
	}

	// Marshal JSON string
	jsonString, err := json.Marshal(cefEvent)
	if err != nil {
		return nil, err
	}

	return jsonString, nil
}

var (
	// Replace non KV spaces with {{SPACE}}
	kvSpace = regexp2.MustCompile(`\s(?!([\w\-]+)\=)`, 0)
	// Unescape CEF fields
	cefEscapeFieldReplacer = strings.NewReplacer(
		"\\\\", "\\",
		"\\|", "|",
		"\\n", "\n",
	)
	// Unescape CEF extensions
	cefEscapeExtensionReplacer = strings.NewReplacer(
		"\\\\", "\\",
		"\\n", "\n",
		"\\=", "=",
	)
)

func CefStringToObject(cefString string) (*CefEvent, error) {
	// Split by CEF separator
	arr := strings.Split(cefString, "|")

	if len(arr) < 8 {
		return nil, fmt.Errorf("invalid CEF format")
	}

	version := ""

	if strings.Contains(arr[0], ":") {
		// Split first field to validate CEF
		validate := strings.Split(arr[0], ":")

		// Validate that it is a valid CEF message
		if validate[0] != "CEF" {
			return nil, fmt.Errorf("invalid CEF format")
		}

		version = validate[1]
	} else {
		if _, err := strconv.Atoi(arr[0]); err != nil {
			return nil, fmt.Errorf("invalid CEF format")
		}
		version = arr[0]
	}

	// Get extensions
	extensions := strings.Join(arr[7:], "|")

	// Replace colons with {{COLON}}
	safeExtensions := strings.ReplaceAll(extensions, ":", "{{COLON}}")
	safeExtensions = strings.ReplaceAll(safeExtensions, `\\=`, "{{EQUAL_ESCAPE_2}}")
	safeExtensions = strings.ReplaceAll(safeExtensions, `\=`, "{{EQUAL_ESCAPE_1}}")

	// Replace non KV spaces with {{SPACE}}
	safeExtensions2, err := kvSpace.Replace(safeExtensions, "{{SPACE}}", -1, -1)

	// Parse extensions in key value format
	keyValueMap, extraKeyValueMap, err := parseKeyValue(safeExtensions2)
	// Handle error
	if err != nil {
		return nil, err
	}

	// Restore colons
	newKeyValueMap := make(map[string]string, 0)
	for k, v := range keyValueMap {
		newKey := strings.ReplaceAll(k, `{{SPACE}}`, " ")
		newKey = strings.ReplaceAll(newKey, `{{EQUAL_ESCAPE_1}}`, `\=`)
		newKey = strings.ReplaceAll(newKey, `{{EQUAL_ESCAPE_2}}`, `\\=`)
		newKey = strings.ReplaceAll(newKey, `{{COLON}}`, ":")

		newValue := strings.ReplaceAll(v, `{{SPACE}}`, " ")
		newValue = strings.ReplaceAll(newValue, `{{EQUAL_ESCAPE_1}}`, `\=`)
		newValue = strings.ReplaceAll(newValue, `{{EQUAL_ESCAPE_2}}`, `\\=`)
		newValue = strings.ReplaceAll(newValue, `{{COLON}}`, ":")
		newValue = strings.TrimSpace(newValue)

		newKeyValueMap[newKey] = newValue
	}

	newExtraKeyValueMap := make(map[string]string, 0)
	for k, v := range extraKeyValueMap {
		newKey := strings.ReplaceAll(k, `{{SPACE}}`, " ")
		newKey = strings.ReplaceAll(newKey, `{{EQUAL_ESCAPE_1}}`, `\=`)
		newKey = strings.ReplaceAll(newKey, `{{EQUAL_ESCAPE_2}}`, `\\=`)
		newKey = strings.ReplaceAll(newKey, `{{COLON}}`, ":")

		newValue := strings.ReplaceAll(v, `{{SPACE}}`, " ")
		newValue = strings.ReplaceAll(newValue, `{{EQUAL_ESCAPE_1}}`, `\=`)
		newValue = strings.ReplaceAll(newValue, `{{EQUAL_ESCAPE_2}}`, `\\=`)
		newValue = strings.ReplaceAll(newValue, `{{COLON}}`, ":")
		newValue = strings.TrimSpace(newValue)

		newExtraKeyValueMap[newKey] = newValue
	}

	// Build CEF event
	cefEvent := &CefEvent{
		Version:            version,
		DeviceVendor:       cefEscapeFieldReplacer.Replace(arr[1]),
		DeviceProduct:      cefEscapeFieldReplacer.Replace(arr[2]),
		DeviceVersion:      cefEscapeFieldReplacer.Replace(arr[3]),
		DeviceEventClassId: cefEscapeFieldReplacer.Replace(arr[4]),
		Name:               cefEscapeFieldReplacer.Replace(arr[5]),
		Severity:           cefEscapeFieldReplacer.Replace(arr[6]),
		Extensions:         newKeyValueMap,
	}

	return cefEvent, nil
}

var (
	// Clear out empty key values
	clearEmpty = regexp.MustCompile("[a-zA-Z0-9]+=[ ]")
	// fix ending
	ending = regexp.MustCompile("[ ][a-zA-Z0-9]+=$")
)

// parseKeyValue will take a key value formatted string and convert it into a extension or extra
func parseKeyValue(event string) (map[string]string, map[string]string, error) {
	// Clear out empty key values
	newEvent := clearEmpty.ReplaceAllString(event, " ")

	// fix ending
	if newEvent[len(newEvent)-1] == '=' {
		newEvent = ending.ReplaceAllString(newEvent, "")
	}

	// Use KeyValue library to parse
	text, list := kv.Parse([]byte(newEvent))

	// If return text then an error occurred during parsing
	if len(text) > 0 {
		return nil, nil, fmt.Errorf(`invalid key value format at: "%s"`, string(text))
	}

	// Convert from list to a map
	extensionsMap := make(map[string]string)
	extraMap := make(map[string]string)

	for i := 0; i < len(list); i += 2 {
		key := cefEscapeExtensionReplacer.Replace(list[i].(string))
		value := cefEscapeExtensionReplacer.Replace(list[i+1].(string))

		if mappedKey, ok := extensionMapping[key]; ok {
			extensionsMap[mappedKey] = value
		} else {
			extraMap[key] = value
		}
	}

	return extensionsMap, extraMap, nil
}

func (c CefEvent) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})

	m["cefVersion"] = c.Version
	m["deviceVendor"] = c.DeviceVendor
	m["deviceProduct"] = c.DeviceProduct
	m["deviceVersion"] = c.DeviceVersion
	m["deviceEventClassId"] = c.DeviceEventClassId
	m["name"] = c.Name
	m["severity"] = c.Severity

	for key := range c.Extensions {
		m[key] = c.Extensions[key]
	}

	return json.Marshal(m)
}
