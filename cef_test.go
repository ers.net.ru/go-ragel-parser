package ragelparser

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseCef(t *testing.T) {
	tests := []struct {
		name    string
		input   []byte
		wantRes *CefEvent
		wantErr error
	}{
		{
			name:  "1",
			input: []byte(`CEF:1|Security|threatmanager|1.0|100|worm successfully stopped|10|src=abc dst=dget d=h|`),
			wantRes: &CefEvent{
				Version:            "1",
				DeviceVendor:       "Security",
				DeviceProduct:      "threatmanager",
				DeviceVersion:      "1.0",
				DeviceEventClassId: "100",
				Name:               "worm successfully stopped",
				Severity:           "10",
				Extensions: map[string]string{
					"src": "abc",
					"dst": "dget",
					"d":   "h",
				},
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := New()
			res, err := ParseCef(m, tt.input)

			assert.Equal(t, tt.wantRes, res)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}
