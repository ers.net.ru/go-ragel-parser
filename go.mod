module gitlab.com/ers.net.ru/go-ragel-parser

go 1.19

require (
	github.com/dlclark/regexp2 v1.7.0
	github.com/jjeffery/kv v0.8.1
	github.com/stretchr/testify v1.8.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/goccy/go-graphviz v0.0.9 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
