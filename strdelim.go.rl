// -*-go-*-
//
// Convert a string to an integer.
//
// To compile:
//
//   ragel -Z -G2 -o strdelim.go strdelim.go.rl
//   go build -o strdelim strdelim.go
//   ./atoi
//
// To show a diagram of your state machine:
//
//   ragel -V -Z -p -o atoi.dot atoi.rl
//   xdot atoi.dot
//

package ragelparser

import (
	"strings"
)

%%{
	machine strdelim;

	action mark { m.pb = m.p }

	sp = ' ';

	esc = '\\';
	vl = '|';

	escaped_w = esc.(sp|vl);

	action add_slice {
		val = append(val,m.data[m.pb:m.p])
    }

	v = (any - escaped_w)+ >mark %add_slice;

	main := v (escaped_w . v)+;

	write data;

	access m.;

	variable cs m.cs;
	variable p m.p;
	variable pe m.pe;
	variable eof m.eof;
	variable data m.data;

}%%


func ParseStrDelim(m *Machine, input []byte) (val [][]byte) {

	m.data = input
	m.pe = len(input)
	m.cs = 0
	m.p = 0
	m.pb = 0
	m.err = nil
	m.eof = len(input)

	val = make([][]byte,0)

	%% write init;
	%% write exec;

	return val
}

func ssToString(in [][]byte) string {

	var b strings.Builder

	for i := 0; i < len(in); i++ {
		b.Write(in[i])
	}

	return b.String()
}