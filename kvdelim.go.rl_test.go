package ragelparser

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseKvDelim(t *testing.T) {
	var tests = []struct {
		name    string
		input   []byte
		want    map[string]string
		wantErr bool
	}{
		{
			name:  "normal",
			input: []byte(`a1=val1 b2=val2`),
			want: map[string]string{
				"a1": "val1",
				"b2": "val2",
			},
		}, {
			name:  `space`,
			input: []byte(`act=aa bb cc dd f=g t3=sadsd`),
			want: map[string]string{
				"act": `aa bb cc dd`,
				"f":   `g`,
			},
		}, {
			name:  `escape vl`,
			input: []byte(`act=abc\|de f=g`),
			want: map[string]string{
				"act": `abc|de`,
				"f":   `g`,
			},
		},
		{
			name:  `escape delim`,
			input: []byte(`act=abc\=de fn=g22`),
			want: map[string]string{
				"act": `abc=de`,
				"fn":  `g22`,
			},
		}, {
			name:  `escape esc`,
			input: []byte(`act=abc\\de f=g`),
			want: map[string]string{
				"act": `abc\de`,
				"f":   `g`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := New()
			res := ParseKvDelim(m, tt.input)

			assert.Equal(t, tt.want, res)
		})
	}
}
