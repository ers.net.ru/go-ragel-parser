package ragelparser

import (
	"bytes"
	"strings"
	"sync"
)

var (
	spool = sync.Pool{
		New: func() interface{} {
			return new(strings.Builder)
		},
	}
)

// AllocBuffer allocates a buffer, or re-uses an existing buffer.
func AllocBuffer() *strings.Builder {
	return spool.Get().(*strings.Builder)
}

// ReleaseBuffer returns a buffer to the pool to be re-used.
func ReleaseBuffer(buf *strings.Builder) {
	if buf != nil {
		buf.Reset()
		spool.Put(buf)
	}
}

const (
	cefVersion         = "cefVersion"
	deviceVendor       = "deviceVendor"
	deviceProduct      = "deviceProduct"
	deviceVersion      = "deviceVersion"
	deviceEventClassId = "deviceEventClassId"
	name               = "name"
	severity           = "severity"
)

func NewCefMap() map[string]string {
	return map[string]string{
		cefVersion:         "",
		deviceVendor:       "",
		deviceProduct:      "",
		deviceVersion:      "",
		deviceEventClassId: "",
		name:               "",
		severity:           "",
	}
}

var cefPrefix = []byte(`CEF:`)
var cefHeaderKey = []string{
	cefVersion,
	deviceVendor,
	deviceProduct,
	deviceVersion,
	deviceEventClassId,
	name,
	severity,
}

func ParseKv(b []byte) map[string]string {
	if !bytes.HasPrefix(b, cefPrefix) {
		return nil
	}

	if !(b[4] == '0' || b[4] == '1') {
		return nil
	}
	if b[5] != '|' {
		return nil
	}

	m := NewCefMap()

	m[cefVersion] = string(b[4])

	//buf := bytes.Buffer{}
	k := AllocBuffer()
	v := AllocBuffer()

	j := 6
	p := j
	h := 1

	he := true
	for he {
		switch b[j] {
		case '\\':
			j++
			switch b[j] {
			case '|', '\\':
				v.Write(b[p : j-1])
				v.WriteByte(b[j])
			}
			p = j + 1
		case '|':
			if v.Len() > 0 {
				v.Write(b[p:j])
				m[cefHeaderKey[h]] = v.String()

				v.Reset()
			} else {
				m[cefHeaderKey[h]] = string(b[p:j])
			}

			h++
			p = j + 1
		}
		j++

		if j == len(b) {
			if h == 6 && p < j {
				m[cefHeaderKey[h]] = string(b[p:])
				return m
			}
			return nil
		}

		if h == 7 {
			he = false
			break
		}
	}

	for i := j; i < len(b); i++ {
		switch b[i] {
		case '=':
			if v.Len() > 0 {
				// write prev pair
				m[k.String()] = v.String()

				k.Reset()
				v.Reset()
			}

			k.Write(b[p:i])
			p = i + 1

		case ' ':
			if v.Len() > 0 && b[p-1] == ' ' {
				v.WriteByte(' ')
			}
			v.Write(b[p:i])
			p = i + 1
		case '\\':
			i++
			switch b[i] {
			case '|', '=', '\\':
				v.Write(b[p : i-1])
				v.WriteByte(b[i])
			}
			p = i + 1
		}

	}
	if k.Len() > 0 {
		v.Write(b[p:])

		// write last pair
		m[k.String()] = v.String()

	}

	ReleaseBuffer(k)
	ReleaseBuffer(v)

	return m
}
