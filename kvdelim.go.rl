// To compile:
//go:generate ragel -Z -G2 -o kvdelim.go kvdelim.go.rl

// To show a diagram of your state machine:
//go:generate ragel -V -Z -p -o kvdelim.dot kvdelim.go.rl

//To show a diagram of your state machine:
//go:generate dot -Tsvg kvdelim.dot -o kvdelim.svg

package ragelparser

import "strings"

%%{
	machine kvdelim;

	action mark { m.pb = m.p }

	action add_k {
		k.Write(m.data[m.pb:m.p])
    }

	action add_v {
		v.Write(m.data[m.pb:m.p])
    }

	action add_ev {
		v.Write(m.data[m.pb+1:m.p])
	}

	action add_pair {
		val[k.String()]=v.String()

		k.Reset()
		v.Reset()
	}

	delim = '=';

	sp = ' ';
	esc = '\\';
	vl = '|';

	e1='\\=';
	e2='\\|';
	e3='\\\\';

	key = alnum+ >mark %add_k;
	key_n = ' f=g';
	# key_n = sp alnum+ delim ;

	ee = (e1|e2|e3) >mark %add_ev;

	ext_f_chunk = (!(e1|e2|e3))+ >mark %add_v;
	# ext_f_chunk = (any >mark %add_v)+ :> e1|e2|e3|key_n;


	val = ext_f_chunk (ee ext_f_chunk)*;

	pair = key delim val %add_pair;

	main := pair (sp pair)*;

	write data;

	access m.;

	variable cs m.cs;
	variable p m.p;
	variable pe m.pe;
	variable eof m.eof;
	variable data m.data;

}%%


func ParseKvDelim(m *Machine, input []byte) (val map[string]string) {

	m.data = input
	m.pe = len(input)
	m.cs = 0
	m.p = 0
	m.pb = 0
	m.err = nil
	m.eof = len(input)

	k := strings.Builder{}
	v := strings.Builder{}

	val = make(map[string]string,0)

	%% write init;
	%% write exec;

	return val
}