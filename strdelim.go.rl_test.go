package ragelparser

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseStrDelim(t *testing.T) {
	var tests = []struct {
		name  string
		input []byte
		//want    [][]byte
		want    string
		wantErr bool
	}{
		{
			name:  "1",
			input: []byte(`123\ 456\|789\ яяя`),
			//want: [][]byte{
			//	[]byte(`123`),
			//	[]byte(`456`),
			//	[]byte(`789`),
			//	[]byte(`яяя`),
			//},
			want: `123456789яяя`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := New()
			res := ssToString(ParseStrDelim(m, tt.input))

			assert.Equal(t, tt.want, res)
		})
	}
}
