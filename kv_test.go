package ragelparser

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseKv(t *testing.T) {
	var tests = []struct {
		name    string
		input   []byte
		want    map[string]string
		wantErr bool
	}{
		{
			name:  "not valid count base fields",
			input: []byte(`CEF:0|Fortinet`),
			want:  nil,
		}, {
			name:  "not valid count base fields",
			input: []byte(`CEF:0|Fortinet|FortiWeb|7.00|20000008|attack`),
			want:  nil,
		},
		{
			name:  "not valid count base fields",
			input: []byte(`CEF:0|Fortinet|FortiWeb|7.00|20000008|attack|`),
			want:  nil,
		},
		{
			name:  "normal",
			input: []byte(`CEF:0|Fortinet|FortiWeb|7.00|20000008|attack|alert`),
			want: map[string]string{

				"cefVersion":         "0",
				"deviceVendor":       "Fortinet",
				"deviceProduct":      "FortiWeb",
				"deviceVersion":      "7.00",
				"deviceEventClassId": "20000008",
				"name":               "attack",
				"severity":           "alert",
			},
		},
		{
			name:  "normal",
			input: []byte(`CEF:0|Fortinet|FortiWeb|7.00|20000008|attack|alert|a1=val1 b2=val2`),
			want: map[string]string{

				"cefVersion":         "0",
				"deviceVendor":       "Fortinet",
				"deviceProduct":      "FortiWeb",
				"deviceVersion":      "7.00",
				"deviceEventClassId": "20000008",
				"name":               "attack",
				"severity":           "alert",
				"a1":                 "val1",
				"b2":                 "val2",
			},
		},
		{
			name:  "header escape vertical line",
			input: []byte(`CEF:0|Fortinet|FortiWeb|7.00|20000008|at \| tack|alert|a1=val1 b2=val2`),
			want: map[string]string{

				"cefVersion":         "0",
				"deviceVendor":       "Fortinet",
				"deviceProduct":      "FortiWeb",
				"deviceVersion":      "7.00",
				"deviceEventClassId": "20000008",
				"name":               `at | tack`,
				"severity":           "alert",
				"a1":                 "val1",
				"b2":                 "val2",
			},
		},
		{
			name:  "header escape backslash",
			input: []byte(`CEF:0|Fortinet|FortiWeb|7.00|20000008|at \\ tack|alert|a1=val1 b2=val2`),
			want: map[string]string{

				"cefVersion":         "0",
				"deviceVendor":       "Fortinet",
				"deviceProduct":      "FortiWeb",
				"deviceVersion":      "7.00",
				"deviceEventClassId": "20000008",
				"name":               `at \ tack`,
				"severity":           "alert",
				"a1":                 "val1",
				"b2":                 "val2",
			},
		},
		//{
		//	name:  `space`,
		//	input: []byte(`act=aa bb cc dd f=g`),
		//	want: map[string]string{
		//		"act": `aa bb cc dd`,
		//		"f":   `g`,
		//	},
		//}, {
		//	name:  `escape vl`,
		//	input: []byte(`act=abc\|de f=g`),
		//	want: map[string]string{
		//		"act": `abc|de`,
		//		"f":   `g`,
		//	},
		//},
		//{
		//	name:  `escape delim`,
		//	input: []byte(`act=abc\=de fn=g22`),
		//	want: map[string]string{
		//		"act": `abc=de`,
		//		"fn":  `g22`,
		//	},
		//}, {
		//	name:  `escape esc`,
		//	input: []byte(`act=abc\\de f=g`),
		//	want: map[string]string{
		//		"act": `abc\de`,
		//		"f":   `g`,
		//	},
		//}, {
		//	name:  `escape esc`,
		//	input: []byte(`a=bc d=⌘Р本語 e=20`),
		//	want: map[string]string{
		//		"a": `bc`,
		//		"d": `⌘Р本語`,
		//		"e": `20`,
		//	},
		//},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res := ParseKv(tt.input)

			assert.Equal(t, tt.want, res)
		})
	}
}
